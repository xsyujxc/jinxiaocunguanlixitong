/*
Navicat MySQL Data Transfer

Source Server         : con_mysql
Source Server Version : 50710
Source Host           : localhost:3306
Source Database       : jxc

Target Server Type    : MYSQL
Target Server Version : 50710
File Encoding         : 65001

Date: 2017-05-22 16:46:53
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for tb_brand
-- ----------------------------
DROP TABLE IF EXISTS `tb_brand`;
CREATE TABLE `tb_brand` (
  `brand_id` int(11) NOT NULL,
  `brand_name` varchar(64) NOT NULL,
  `provision1` varchar(255) DEFAULT NULL,
  `provision2` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`brand_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tb_brand
-- ----------------------------
INSERT INTO `tb_brand` VALUES ('1', 'BOSCH/博世', null, null);
INSERT INTO `tb_brand` VALUES ('2', 'HMCT/哈量', null, null);
INSERT INTO `tb_brand` VALUES ('3', 'POINT/蓝点', null, null);
INSERT INTO `tb_brand` VALUES ('4', 'ENDURA/力易得', null, null);
INSERT INTO `tb_brand` VALUES ('5', 'SHAOWEI/少威', null, null);
INSERT INTO `tb_brand` VALUES ('6', 'HONEYWELL/霍尼韦尔', null, null);
INSERT INTO `tb_brand` VALUES ('7', 'SIEMENS/西门子', null, null);
INSERT INTO `tb_brand` VALUES ('8', 'TANKO/天钢', null, null);
INSERT INTO `tb_brand` VALUES ('9', 'ZKH/震坤行', null, null);

-- ----------------------------
-- Table structure for tb_category
-- ----------------------------
DROP TABLE IF EXISTS `tb_category`;
CREATE TABLE `tb_category` (
  `category_id` int(11) NOT NULL COMMENT '标识列',
  `category_name` varchar(64) NOT NULL COMMENT '类别名称',
  `provision1` varchar(64) DEFAULT NULL,
  `provision2` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`category_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tb_category
-- ----------------------------
INSERT INTO `tb_category` VALUES ('1', '焊接', null, null);
INSERT INTO `tb_category` VALUES ('2', '工具', null, null);
INSERT INTO `tb_category` VALUES ('3', '仪器仪表', null, null);
INSERT INTO `tb_category` VALUES ('4', '电器', null, null);
INSERT INTO `tb_category` VALUES ('5', '金属加工', null, null);
INSERT INTO `tb_category` VALUES ('6', '搬运存储', null, null);
INSERT INTO `tb_category` VALUES ('7', '动力传动', null, null);

-- ----------------------------
-- Table structure for tb_employee
-- ----------------------------
DROP TABLE IF EXISTS `tb_employee`;
CREATE TABLE `tb_employee` (
  `emp_id` int(11) NOT NULL,
  `emp_name` varchar(64) NOT NULL,
  `password` varchar(64) NOT NULL,
  `station` int(11) NOT NULL,
  `provision1` varchar(64) DEFAULT NULL,
  `provision2` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`emp_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tb_employee
-- ----------------------------
INSERT INTO `tb_employee` VALUES ('1', 'Cristiano', '123', '1', '', null);

-- ----------------------------
-- Table structure for tb_exwarehouse_record
-- ----------------------------
DROP TABLE IF EXISTS `tb_exwarehouse_record`;
CREATE TABLE `tb_exwarehouse_record` (
  `exwarehouse_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '出库记录编号',
  `storage_id` int(11) NOT NULL COMMENT '入库批次编号约束',
  `sale_item_id` int(11) DEFAULT NULL COMMENT '该批次货品出库用于处理哪一单销售记录',
  `count` int(11) NOT NULL COMMENT '出库数量',
  `date` datetime NOT NULL COMMENT '出库日期',
  `inspector` int(11) NOT NULL COMMENT '出库操作员',
  `provision1` varchar(255) DEFAULT NULL,
  `provision2` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`exwarehouse_id`),
  KEY `for_inspector_id` (`inspector`),
  KEY `forsale_item` (`sale_item_id`),
  KEY `forsorage_id` (`storage_id`),
  CONSTRAINT `for_inspector_id` FOREIGN KEY (`inspector`) REFERENCES `tb_employee` (`emp_id`),
  CONSTRAINT `forsale_item` FOREIGN KEY (`sale_item_id`) REFERENCES `tb_sale_item` (`sale_item_id`),
  CONSTRAINT `forsorage_id` FOREIGN KEY (`storage_id`) REFERENCES `tb_storage_record` (`storage_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tb_exwarehouse_record
-- ----------------------------
INSERT INTO `tb_exwarehouse_record` VALUES ('1', '1', '1', '3', '2017-05-20 14:03:05', '1', null, null);
INSERT INTO `tb_exwarehouse_record` VALUES ('2', '5', '1', '3', '2017-05-22 13:45:14', '1', null, null);
INSERT INTO `tb_exwarehouse_record` VALUES ('3', '2', '2', '5', '2017-05-22 14:09:00', '1', null, null);
INSERT INTO `tb_exwarehouse_record` VALUES ('4', '3', '2', '2', '2017-05-22 14:34:03', '1', null, null);

-- ----------------------------
-- Table structure for tb_production
-- ----------------------------
DROP TABLE IF EXISTS `tb_production`;
CREATE TABLE `tb_production` (
  `production_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '商品id',
  `sub_category_id` int(11) NOT NULL COMMENT '产品类别',
  `production_name` varchar(64) NOT NULL COMMENT '名称',
  `model_num` varchar(64) NOT NULL COMMENT '型号',
  `size` varchar(64) DEFAULT NULL COMMENT '规格',
  `materail` varchar(64) DEFAULT NULL COMMENT '材质',
  `application` varchar(128) DEFAULT NULL COMMENT '用途',
  `provision1` varchar(128) DEFAULT NULL COMMENT '备注1',
  `provision2` varchar(255) DEFAULT NULL COMMENT '备注2',
  PRIMARY KEY (`production_id`,`model_num`),
  KEY `for_category_id` (`sub_category_id`),
  KEY `production_id` (`production_id`),
  CONSTRAINT `for_category_id` FOREIGN KEY (`sub_category_id`) REFERENCES `tb_sub_category` (`sub_category_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tb_production
-- ----------------------------
INSERT INTO `tb_production` VALUES ('1', '1', '紫铜焊条', 'Cu170', null, null, null, null, null);
INSERT INTO `tb_production` VALUES ('2', '4', '外热式电烙铁', 'DEL-30', null, null, null, null, null);
INSERT INTO `tb_production` VALUES ('3', '13', '接触式测温仪', 'FLUKE-52-II', null, null, null, null, null);
INSERT INTO `tb_production` VALUES ('4', '16', '速度控制继电器', 'RM35S0MW', null, null, null, '', null);
INSERT INTO `tb_production` VALUES ('5', '20', '厚壁千分尺', '719-02', null, null, null, null, null);

-- ----------------------------
-- Table structure for tb_purchase_item
-- ----------------------------
DROP TABLE IF EXISTS `tb_purchase_item`;
CREATE TABLE `tb_purchase_item` (
  `item_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '标识列',
  `item_production_id` int(11) NOT NULL COMMENT '商品id约束',
  `purchase_list_id` int(11) NOT NULL COMMENT '采购单id约束',
  `order_count` int(11) NOT NULL COMMENT '计划采购数量',
  `unit_id` int(11) NOT NULL DEFAULT '1' COMMENT '采购货品单位',
  `suplier_id` int(11) DEFAULT NULL,
  `brand_id` int(11) DEFAULT NULL COMMENT '品牌id',
  `item_status` varchar(8) NOT NULL COMMENT '当前采购条目状态(已采购完成/未采购完成)',
  `provision1` varchar(64) DEFAULT NULL,
  `provision2` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`item_id`),
  KEY `for_production` (`item_production_id`),
  KEY `for_brand_id` (`brand_id`),
  KEY `for_supplier` (`suplier_id`),
  KEY `for_purchase_list` (`purchase_list_id`),
  CONSTRAINT `for_brand_id` FOREIGN KEY (`brand_id`) REFERENCES `tb_brand` (`brand_id`),
  CONSTRAINT `for_production` FOREIGN KEY (`item_production_id`) REFERENCES `tb_production` (`production_id`),
  CONSTRAINT `for_purchase_list` FOREIGN KEY (`purchase_list_id`) REFERENCES `tb_purchase_list` (`purchase_id`),
  CONSTRAINT `for_supplier` FOREIGN KEY (`suplier_id`) REFERENCES `tb_supplier` (`supplier_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tb_purchase_item
-- ----------------------------
INSERT INTO `tb_purchase_item` VALUES ('1', '1', '1', '12', '1', null, '5', '0', null, null);
INSERT INTO `tb_purchase_item` VALUES ('2', '3', '1', '10', '1', null, '6', '0', null, null);
INSERT INTO `tb_purchase_item` VALUES ('3', '4', '1', '15', '1', null, null, '0', null, null);

-- ----------------------------
-- Table structure for tb_purchase_list
-- ----------------------------
DROP TABLE IF EXISTS `tb_purchase_list`;
CREATE TABLE `tb_purchase_list` (
  `purchase_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '订购单标识列',
  `commander` int(11) NOT NULL COMMENT '下单人员',
  `date` datetime(6) NOT NULL COMMENT '下单日期',
  `status` bit(1) NOT NULL DEFAULT b'1' COMMENT '当前采购单状态(“有效”/“无效”)',
  `provision1` varchar(64) DEFAULT NULL,
  `provision2` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`purchase_id`),
  KEY `for_commander` (`commander`),
  CONSTRAINT `for_commander` FOREIGN KEY (`commander`) REFERENCES `tb_employee` (`emp_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tb_purchase_list
-- ----------------------------
INSERT INTO `tb_purchase_list` VALUES ('1', '1', '2017-05-18 18:06:53.000000', '', null, null);

-- ----------------------------
-- Table structure for tb_purchase_record
-- ----------------------------
DROP TABLE IF EXISTS `tb_purchase_record`;
CREATE TABLE `tb_purchase_record` (
  `record_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '标识列',
  `item_id` int(11) NOT NULL COMMENT '采购条目',
  `real_count` int(11) NOT NULL COMMENT '实际采购数量',
  `unit_price` double NOT NULL COMMENT '采购单价',
  `supplier_id` int(11) NOT NULL COMMENT '供应商id',
  `brand_id` int(11) NOT NULL COMMENT '品牌id',
  `purchase_agent` int(11) NOT NULL COMMENT '采购员',
  `date` datetime NOT NULL COMMENT '采购日期',
  `provision1` varchar(64) DEFAULT NULL,
  `provision2` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`record_id`),
  KEY `for_supplier_id` (`supplier_id`),
  KEY `for_agent` (`purchase_agent`),
  KEY `for_brand` (`brand_id`),
  KEY `for_purches_item_id` (`item_id`),
  CONSTRAINT `for_agent` FOREIGN KEY (`purchase_agent`) REFERENCES `tb_employee` (`emp_id`),
  CONSTRAINT `for_brand` FOREIGN KEY (`brand_id`) REFERENCES `tb_brand` (`brand_id`),
  CONSTRAINT `for_purches_item_id` FOREIGN KEY (`item_id`) REFERENCES `tb_purchase_item` (`item_id`),
  CONSTRAINT `for_supplier_id` FOREIGN KEY (`supplier_id`) REFERENCES `tb_supplier` (`supplier_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tb_purchase_record
-- ----------------------------
INSERT INTO `tb_purchase_record` VALUES ('1', '1', '12', '15', '1', '1', '1', '2017-05-20 12:35:06', null, null);
INSERT INTO `tb_purchase_record` VALUES ('2', '2', '7', '20', '1', '2', '1', '2017-05-20 19:48:50', null, null);
INSERT INTO `tb_purchase_record` VALUES ('3', '2', '2', '21', '1', '2', '1', '2017-05-20 21:31:15', null, null);
INSERT INTO `tb_purchase_record` VALUES ('4', '3', '10', '30', '1', '1', '1', '2017-05-22 13:32:08', null, null);
INSERT INTO `tb_purchase_record` VALUES ('5', '3', '5', '34', '1', '7', '1', '2017-05-22 15:28:48', null, null);

-- ----------------------------
-- Table structure for tb_sale_item
-- ----------------------------
DROP TABLE IF EXISTS `tb_sale_item`;
CREATE TABLE `tb_sale_item` (
  `sale_item_id` int(11) NOT NULL AUTO_INCREMENT,
  `list_id` int(11) NOT NULL,
  `production_id` int(11) NOT NULL,
  `count` int(11) NOT NULL,
  `unit_id` int(11) NOT NULL,
  `brand_id` int(11) NOT NULL,
  `unit_price` double DEFAULT NULL,
  `provision1` varchar(255) DEFAULT NULL,
  `provision2` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`sale_item_id`),
  KEY `brand` (`brand_id`),
  KEY `for_product_id` (`production_id`) USING BTREE,
  KEY `unit_id` (`unit_id`),
  KEY `sale_list` (`list_id`),
  CONSTRAINT `brand` FOREIGN KEY (`brand_id`) REFERENCES `tb_brand` (`brand_id`),
  CONSTRAINT `product_id` FOREIGN KEY (`production_id`) REFERENCES `tb_production` (`production_id`),
  CONSTRAINT `sale_list` FOREIGN KEY (`list_id`) REFERENCES `tb_sale_list` (`sale_list_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of tb_sale_item
-- ----------------------------
INSERT INTO `tb_sale_item` VALUES ('1', '1', '4', '6', '1', '1', '50', null, null);
INSERT INTO `tb_sale_item` VALUES ('2', '1', '3', '7', '1', '2', '35', null, null);

-- ----------------------------
-- Table structure for tb_sale_list
-- ----------------------------
DROP TABLE IF EXISTS `tb_sale_list`;
CREATE TABLE `tb_sale_list` (
  `sale_list_id` int(10) NOT NULL AUTO_INCREMENT COMMENT '标识列',
  `customer` varchar(255) NOT NULL COMMENT '订单客户',
  `date` datetime NOT NULL COMMENT '下单日期',
  `status` bit(1) NOT NULL DEFAULT b'0' COMMENT '订单状态',
  `provision1` varchar(255) DEFAULT NULL,
  `provision2` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`sale_list_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tb_sale_list
-- ----------------------------
INSERT INTO `tb_sale_list` VALUES ('1', 'Messi', '2017-05-20 19:07:13', '\0', null, null);

-- ----------------------------
-- Table structure for tb_storage_record
-- ----------------------------
DROP TABLE IF EXISTS `tb_storage_record`;
CREATE TABLE `tb_storage_record` (
  `storage_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '入库批次编号',
  `purchase_record_id` int(11) NOT NULL COMMENT '采购记录id约束',
  `init_count` int(11) NOT NULL,
  `date` datetime NOT NULL COMMENT '入库日期',
  `inspector` int(11) NOT NULL COMMENT '验收/仓库管理人员',
  `provision1` varchar(64) DEFAULT NULL,
  `provision2` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`storage_id`),
  KEY `for_inspector` (`inspector`),
  KEY `for_purchase_record_id` (`purchase_record_id`),
  CONSTRAINT `for_inspector` FOREIGN KEY (`inspector`) REFERENCES `tb_employee` (`emp_id`),
  CONSTRAINT `for_purchase_record_id` FOREIGN KEY (`purchase_record_id`) REFERENCES `tb_purchase_record` (`record_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tb_storage_record
-- ----------------------------
INSERT INTO `tb_storage_record` VALUES ('1', '1', '12', '2017-05-20 12:37:21', '1', null, null);
INSERT INTO `tb_storage_record` VALUES ('2', '2', '5', '2017-05-20 20:15:26', '1', null, null);
INSERT INTO `tb_storage_record` VALUES ('3', '2', '2', '2017-05-20 21:34:02', '1', null, null);
INSERT INTO `tb_storage_record` VALUES ('4', '3', '1', '2017-05-20 21:39:36', '1', null, null);
INSERT INTO `tb_storage_record` VALUES ('5', '4', '10', '2017-05-22 13:34:39', '1', null, null);

-- ----------------------------
-- Table structure for tb_sub_category
-- ----------------------------
DROP TABLE IF EXISTS `tb_sub_category`;
CREATE TABLE `tb_sub_category` (
  `sub_category_id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_category_id` int(11) NOT NULL,
  `category_name` varchar(64) NOT NULL,
  `provision1` varchar(128) DEFAULT NULL,
  `provision2` varchar(128) DEFAULT NULL,
  PRIMARY KEY (`sub_category_id`),
  KEY `for_par_category_id` (`parent_category_id`),
  CONSTRAINT `for_par_category_id` FOREIGN KEY (`parent_category_id`) REFERENCES `tb_category` (`category_id`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tb_sub_category
-- ----------------------------
INSERT INTO `tb_sub_category` VALUES ('1', '1', '焊条', null, null);
INSERT INTO `tb_sub_category` VALUES ('2', '1', '焊丝', null, null);
INSERT INTO `tb_sub_category` VALUES ('3', '1', '焊枪', null, null);
INSERT INTO `tb_sub_category` VALUES ('4', '1', '电烙铁', null, null);
INSERT INTO `tb_sub_category` VALUES ('5', '2', '钻头', null, null);
INSERT INTO `tb_sub_category` VALUES ('6', '2', '电钻', null, null);
INSERT INTO `tb_sub_category` VALUES ('7', '2', '凿子', null, null);
INSERT INTO `tb_sub_category` VALUES ('8', '2', '钉枪', null, null);
INSERT INTO `tb_sub_category` VALUES ('9', '2', '尺子', null, null);
INSERT INTO `tb_sub_category` VALUES ('10', '2', '扭力工具', null, null);
INSERT INTO `tb_sub_category` VALUES ('11', '3', '张力计', null, null);
INSERT INTO `tb_sub_category` VALUES ('12', '3', '变送器', null, null);
INSERT INTO `tb_sub_category` VALUES ('13', '3', '测温仪', null, null);
INSERT INTO `tb_sub_category` VALUES ('14', '3', '侧厚仪', null, null);
INSERT INTO `tb_sub_category` VALUES ('15', '3', '硬度计', null, null);
INSERT INTO `tb_sub_category` VALUES ('16', '4', '接线板', null, null);
INSERT INTO `tb_sub_category` VALUES ('17', '4', '变频器', null, null);
INSERT INTO `tb_sub_category` VALUES ('18', '4', '电器箱', null, null);
INSERT INTO `tb_sub_category` VALUES ('19', '4', '定时器', null, null);
INSERT INTO `tb_sub_category` VALUES ('20', '5', '千分尺', null, null);
INSERT INTO `tb_sub_category` VALUES ('21', '5', '千分表', null, null);
INSERT INTO `tb_sub_category` VALUES ('22', '5', '卡尺', null, null);
INSERT INTO `tb_sub_category` VALUES ('23', '5', '卡规', null, null);
INSERT INTO `tb_sub_category` VALUES ('24', '6', '千斤顶', null, null);
INSERT INTO `tb_sub_category` VALUES ('25', '6', '脚轮', null, null);
INSERT INTO `tb_sub_category` VALUES ('26', '6', '工业秤', null, null);

-- ----------------------------
-- Table structure for tb_supplier
-- ----------------------------
DROP TABLE IF EXISTS `tb_supplier`;
CREATE TABLE `tb_supplier` (
  `supplier_id` int(11) NOT NULL COMMENT '标识列',
  `supplier_name` varchar(64) NOT NULL COMMENT '供应商名称',
  PRIMARY KEY (`supplier_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tb_supplier
-- ----------------------------
INSERT INTO `tb_supplier` VALUES ('1', '供应商1');

-- ----------------------------
-- Table structure for tb_unit
-- ----------------------------
DROP TABLE IF EXISTS `tb_unit`;
CREATE TABLE `tb_unit` (
  `unit_id` int(11) NOT NULL AUTO_INCREMENT,
  `unit_type` varchar(64) NOT NULL,
  `provision1` varchar(255) DEFAULT NULL,
  `provision2` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`unit_id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tb_unit
-- ----------------------------
INSERT INTO `tb_unit` VALUES ('1', '个', null, null);
INSERT INTO `tb_unit` VALUES ('2', '盒', null, null);
INSERT INTO `tb_unit` VALUES ('3', '箱', null, null);
INSERT INTO `tb_unit` VALUES ('4', '斤', null, null);
INSERT INTO `tb_unit` VALUES ('5', '把', null, null);
INSERT INTO `tb_unit` VALUES ('6', '包', null, null);
INSERT INTO `tb_unit` VALUES ('7', '块', null, null);
INSERT INTO `tb_unit` VALUES ('8', '米', null, null);
INSERT INTO `tb_unit` VALUES ('9', '捆', null, null);
INSERT INTO `tb_unit` VALUES ('10', '瓶', null, null);
INSERT INTO `tb_unit` VALUES ('11', '卷', null, null);
INSERT INTO `tb_unit` VALUES ('12', '支', null, null);
INSERT INTO `tb_unit` VALUES ('13', '副', null, null);

-- ----------------------------
-- View structure for 仓库货品信息表
-- ----------------------------
DROP VIEW IF EXISTS `仓库货品信息表`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `仓库货品信息表` AS select `jxc`.`tb_storage_record`.`storage_id` AS `入库编号`,`jxc`.`tb_purchase_item`.`item_production_id` AS `产品_id`,`jxc`.`tb_purchase_record`.`record_id` AS `采购记录_id`,`jxc`.`tb_purchase_record`.`brand_id` AS `品牌_id`,`jxc`.`tb_purchase_record`.`supplier_id` AS `供应商_id`,`jxc`.`tb_purchase_record`.`unit_price` AS `货品采购单价`,`jxc`.`tb_storage_record`.`init_count` AS `入库数量`,`jxc`.`tb_purchase_item`.`unit_id` AS `单位_id`,`jxc`.`tb_storage_record`.`date` AS `入库日期`,(`jxc`.`tb_storage_record`.`init_count` - `temp1`.`be_saled`) AS `该批货品剩余数量` from (((((select `jxc`.`tb_storage_record`.`storage_id` AS `storage_id`,sum(`jxc`.`tb_exwarehouse_record`.`count`) AS `be_saled` from (`jxc`.`tb_storage_record` left join `jxc`.`tb_exwarehouse_record` on((`jxc`.`tb_exwarehouse_record`.`storage_id` = `jxc`.`tb_storage_record`.`storage_id`))) group by `jxc`.`tb_storage_record`.`storage_id`)) `temp1` join `jxc`.`tb_storage_record`) join `jxc`.`tb_purchase_record`) join `jxc`.`tb_purchase_item`) where ((`jxc`.`tb_storage_record`.`storage_id` = `temp1`.`storage_id`) and (`jxc`.`tb_purchase_record`.`record_id` = `jxc`.`tb_storage_record`.`purchase_record_id`) and (`jxc`.`tb_purchase_item`.`item_id` = `jxc`.`tb_purchase_record`.`item_id`)) ;

-- ----------------------------
-- View structure for 待入库货品信息表
-- ----------------------------
DROP VIEW IF EXISTS `待入库货品信息表`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `待入库货品信息表` AS select `jxc`.`tb_purchase_record`.`record_id` AS `采购记录编号`,`jxc`.`tb_purchase_item`.`item_production_id` AS `采购货品_id`,`jxc`.`tb_purchase_record`.`date` AS `采购日期`,`jxc`.`tb_purchase_record`.`brand_id` AS `品牌_id`,`jxc`.`tb_purchase_record`.`supplier_id` AS `供应商_id`,`jxc`.`tb_purchase_item`.`unit_id` AS `单位_id`,`jxc`.`tb_purchase_record`.`unit_price` AS `采购单价`,`jxc`.`tb_purchase_record`.`real_count` AS `采购数量`,`temp1`.`已入库数量` AS `已入库数量`,(`jxc`.`tb_purchase_record`.`real_count` - `temp1`.`已入库数量`) AS `待入库数量` from ((((select `jxc`.`tb_purchase_record`.`record_id` AS `purchase_record_id`,sum(`jxc`.`tb_storage_record`.`init_count`) AS `已入库数量` from (`jxc`.`tb_purchase_record` left join `jxc`.`tb_storage_record` on((`jxc`.`tb_storage_record`.`purchase_record_id` = `jxc`.`tb_purchase_record`.`record_id`))) group by `jxc`.`tb_purchase_record`.`record_id`)) `temp1` join `jxc`.`tb_purchase_record`) join `jxc`.`tb_purchase_item`) where ((`jxc`.`tb_purchase_record`.`record_id` = `temp1`.`purchase_record_id`) and (`jxc`.`tb_purchase_item`.`item_id` = `jxc`.`tb_purchase_record`.`item_id`)) ;

-- ----------------------------
-- View structure for 待处理订购条目信息表
-- ----------------------------
DROP VIEW IF EXISTS `待处理订购条目信息表`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `待处理订购条目信息表` AS select `temp2`.`sale_item_id` AS `订购条目编号`,`temp2`.`production_id` AS `订购产品_id`,`temp2`.`brand_id` AS `品牌_id`,`temp2`.`unit_price` AS `出售单价`,`temp2`.`count` AS `用户订购数量`,`temp2`.`unit_id` AS `单位_id`,`temp1`.`已出库数量` AS `已出库数量`,(`temp2`.`count` - `temp1`.`已出库数量`) AS `待出库数量` from (`jxc`.`tb_sale_item` `temp2` left join (select `jxc`.`tb_exwarehouse_record`.`sale_item_id` AS `sale_item_id`,sum(`jxc`.`tb_exwarehouse_record`.`count`) AS `已出库数量` from `jxc`.`tb_exwarehouse_record` group by `jxc`.`tb_exwarehouse_record`.`sale_item_id`) `temp1` on((`temp2`.`sale_item_id` = `temp1`.`sale_item_id`))) ;

-- ----------------------------
-- View structure for 待采购货品信息表
-- ----------------------------
DROP VIEW IF EXISTS `待采购货品信息表`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `待采购货品信息表` AS select `jxc`.`tb_purchase_item`.`purchase_list_id` AS `采购单编号`,`jxc`.`tb_purchase_item`.`item_id` AS `计划采购条目编号`,`jxc`.`tb_purchase_item`.`item_production_id` AS `采购货品_id`,`jxc`.`tb_purchase_item`.`unit_id` AS `货品采购单位_id`,`jxc`.`tb_purchase_item`.`brand_id` AS `品牌要求_id`,`jxc`.`tb_purchase_item`.`suplier_id` AS `供应商要求_id`,`jxc`.`tb_purchase_item`.`order_count` AS `计划采购数量`,`temp1`.`已采购数量` AS `已采购数量`,(`jxc`.`tb_purchase_item`.`order_count` - `temp1`.`已采购数量`) AS `待采购数量` from ((select `jxc`.`tb_purchase_item`.`item_id` AS `item_id`,sum(`jxc`.`tb_purchase_record`.`real_count`) AS `已采购数量` from (`jxc`.`tb_purchase_item` left join `jxc`.`tb_purchase_record` on((`jxc`.`tb_purchase_item`.`item_id` = `jxc`.`tb_purchase_record`.`item_id`))) group by `jxc`.`tb_purchase_item`.`item_id`) `temp1` join `jxc`.`tb_purchase_item`) where (`jxc`.`tb_purchase_item`.`item_id` = `temp1`.`item_id`) ;

-- ----------------------------
-- View structure for 销售利润信息表
-- ----------------------------
DROP VIEW IF EXISTS `销售利润信息表`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `销售利润信息表` AS select `temp3`.`出库编号` AS `出库编号`,`jxc`.`tb_purchase_item`.`item_production_id` AS `出库产品_id`,`jxc`.`tb_purchase_record`.`brand_id` AS `品牌_id`,`temp3`.`出库数量` AS `出库数量`,`jxc`.`tb_purchase_record`.`record_id` AS `进购条目编号`,`jxc`.`tb_purchase_record`.`unit_price` AS `进购单价`,`jxc`.`tb_sale_item`.`list_id` AS `订购单编号`,`temp3`.`订购条目编号` AS `订购条目编号`,`temp3`.`出售单价` AS `出售单价`,((`temp3`.`出售单价` - `jxc`.`tb_purchase_record`.`unit_price`) * `temp3`.`出库数量`) AS `该批出库货品利润` from (((((select `temp2`.`exwarehouse_id` AS `出库编号`,`temp2`.`count` AS `出库数量`,`temp2`.`date` AS `出库日期`,`temp2`.`storage_id` AS `入库编号`,`temp1`.`sale_item_id` AS `订购条目编号`,`temp1`.`unit_price` AS `出售单价` from (`jxc`.`tb_exwarehouse_record` `temp2` left join `jxc`.`tb_sale_item` `temp1` on((`temp1`.`sale_item_id` = `temp2`.`sale_item_id`)))) `temp3` join `jxc`.`tb_storage_record`) join `jxc`.`tb_purchase_record`) join `jxc`.`tb_purchase_item`) join `jxc`.`tb_sale_item`) where ((`jxc`.`tb_purchase_record`.`record_id` = `jxc`.`tb_storage_record`.`purchase_record_id`) and (`jxc`.`tb_storage_record`.`storage_id` = `temp3`.`入库编号`) and (`jxc`.`tb_purchase_item`.`item_id` = `jxc`.`tb_purchase_record`.`item_id`) and (`temp3`.`订购条目编号` = `jxc`.`tb_sale_item`.`sale_item_id`)) ;
